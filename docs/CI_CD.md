# Continuous Integration and Continuous Deployment

The GitLab pipeline configuration can be found in [.gitlab-ci.yml](./../.gitlab-ci.yml) The pipeline runs on each push to the repository. It contains the following stages:

1. build - make a clean install of all project and service dependencies
2. test - runs `npm test`
