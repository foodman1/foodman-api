# Services

[[_TOC_]]

## About Code Duplication and Code Documentation across Services

Each service is written in JavaScript and has the same directory structure. Therefore each service also has common functionalities (e.g. connecting to the cloud database). To keep each service as independent as possible from other services the same code snipped may occur in multiple service.

The code itself is only documented where it's necessary. This means only code snippets, that deviate from expected behaviour or perform some sort of :sparkles: magic :sparkles: should be documented. The code itself should be written in a kind of way, that it's readable by using expressive variable and function names.

However, to give new developers a good entry it's recommended to start with the `/src/index.js` in the [category-service](./../packages/category-service). This service is extensively documented and should give a good understanding on how a service is structured and how functionality is implemented and tested.

## Service Dependencies

Service dependencies are necessary to execute the microservice itself.

- [express](https://expressjs.com/de/) - core framework used to create API endpoints and manage middlewares.
- [mongoose](https://mongoosejs.com/) - to manage MongoDB connections, reading and writing objects into the database and to model object and application data
- [express-openapi-validator](https://www.npmjs.com/package/express-openapi-validator) - validates incoming requests and outgoing responses against the OpenAPI specification
- [morgan](https://www.npmjs.com/package/morgan) - logging middleware
- [dotenv](https://www.npmjs.com/package/dotenv) - to read in evironment variables from the project's `.env` file

Further dependencies are used for development:

- [@babel/core, @babel(node), @babel/preset-env, babel-jest](https://babeljs.io/) - to use latest ECMAScript features
- [nodemon](https://www.npmjs.com/package/nodemon) - auto restarting node runtime one file changes
- [jest](https://jestjs.io/) - core framework used for testing
- [supertest](https://www.npmjs.com/package/supertest) - to mock the express app for testing
- [mongodb-memory-server](https://github.com/nodkz/mongodb-memory-server) - in-memory MongoDB used for testing
- [cross-env](https://www.npmjs.com/package/cross-env) - to provide operating system independent environment variablescross en

## Workflow of Developing a new Endpoint

The best-case workflow to develop an endpoint using is test-driven development. Below used paths are relative to the service directory.

1.  (`./src/config/`) specify the endpoint in the OpenAPI specification
2.  (`./src/tests/`) write tests for the endpoint
3.  (_optional_ `./src/utils/`) create a new utility (functionality, which is neither a middleware nor a controller)
4.  (_optional_ `./src/models/`) create a new data model
5.  (_optional_ `./src/middleware/`) create a new middleware
6.  (`./src/controllers/`) implement the functionality of your endpoint in the controller
7.  (`./src/routes/`) map the desired route to middlewares and the controller
8.  Run tests using `npm test`
9.  **On failure:** fix bugs
10. **On success:** refer to the new endpoint in the global OpenAPI specification and bundle a new specification using `npm run openapi:bundle`

## Directory Structure

```
./packages/<service>/
├── src
│   ├── config
│   ├── controllers
│   ├── middlewares
│   ├── models
│   ├── routes
│   ├── tests
│   ├── utils
│   ├── app.js
│   └── index.js
├── .dockerignore
├── Dockerfile
├── babel.config.js
├── jest.config.js
├── package-lock.json
└── package.json
```

_(generated with `tree -a -L 2 -I node_modules --dirsfirst ./packages/category-service/`, edited)_

| Directory or File  | Description                                                              |
| ------------------ | ------------------------------------------------------------------------ |
| `/src/config`      | keeps service OpenAPI specification specifications                       |
| `/src/controllers` | keeps controllers (functionality of an endpoint)                         |
| `/src/middlewares` | keeps middlewares                                                        |
| `/src/models`      | keeps models (database object description)                               |
| `/src/routes`      | keeps routes (path used to access endpoint)                              |
| `/src/tests`       | keeps tests                                                              |
| `/src/utils`       | keeps utils (functionality, which is neihter a controller or middleware) |
| `/src/index.js`    | entrypoint of a service                                                  |
| `/src/app.js`      | express application documentation                                        |
| `package.json`     | npm service metadata and dependencies                                    |
