# Project

The project is a monorepository for all microservices, which belong to the API.

[[_TOC_]]

## Project Dependencies

Project dependencies are not necessary for executing the microservices itself. They provide useful tooling and help to keep the project clean.

- [Lerna](https://lerna.js.org/) - Tool for managing JavaScript projects with multiple packages
- [prettier](https://prettier.io/) - Oppinionated Code Formatter
- [swagger-cli](https://www.npmjs.com/package/swagger-cli) - OpenAPI specification validator and bundler

## Docker-Compose File

The `docker-compose.yml` is the entry point for docker-compose to spin up all services in an isolated environment. The compose file consists of the following services:

1. [Traefik](https://doc.traefik.io/traefik/) - A dynamic router used to map requests to the matching docker container.
2. Microservices - Each service, that can be found under [/packages](./../packages)
3. [ReDoc](https://github.com/Redocly/redoc) - A tool for viewing OpenAPI-generated referenec documentations

Each microservice ...

- reads in the project-wide `.env` environment file,
- is build from it's own `/packages/<service>/Dockerfile`,
- exposes the ports required for the service and database to a random free host port and
- sets the traefik configuration via the `labels` property.

For an production-like environment the start command `npm start` is used. For the development environment the docker-compose configuration is overwritten with `docker-compose.dev.yml`. This sets the start command to `npm run start:dev` and mounts the service directories into the docker container.

## Database

The underlying database of all microservices is MongoDB. To persist data between development environments and different developers a managed [MongoDB Atlas](https://www.mongodb.com/cloud/atlas) cluster is used.

> MongoDB Atlas provides an easy way to host and manage your data in the cloud.

This project currently runs on the free tier. Each service is responsible for it's own unique set of collections.

## NPM Scripts

| npm script         | Description                                                                         |
| ------------------ | ----------------------------------------------------------------------------------- |
| `start`            | spins up production-like environment                                                |
| `start:dev`        | spins up development environment                                                    |
| `stop`             | stops currently running environment                                                 |
| `test`             | runs tests in all packages                                                          |
| `openapi:bundle`   | bundles `/config/openapi.yml` OpenAPI specification to `/config/bundle.openapi.yml` |
| `openapi:validate` | validates `/config/openapi.yml` OpenAPI specification                               |

## Directory Structure

```
.
├── config
│   ├── bundle.openapi.yml
│   └── openapi.yml
├── docs
│   └── ...
├── packages
│   ├── cart-service
│   ├── category-service
│   ├── order-service
│   ├── payment-service
│   ├── product-service
│   └── table-service
├── .editorconfig
├── .env
├── .gitignore
├── .gitlab-ci.yml
├── .prettierrc
├── README.md
├── docker-compose.dev.yml
├── docker-compose.yml
├── lerna.json
├── package-lock.json
└── package.json
```

_(generated with `tree -a -L 2 -I node_modules --dirsfirst .`, edited)_

| Directory or File            | Description                                                                                                            |
| ---------------------------- | ---------------------------------------------------------------------------------------------------------------------- |
| `/config`                    | keeps project-wide OpenAPI specification specifications                                                                |
| `/config/openapi.yml`        | entry point for OpenAPI specification, which references the specification of each service                              |
| `/config/bundle.openapi.yml` | bundled OpenAPI specification after running `npm run openapi:bundle`                                                   |
| `/docs`                      | Markdown documentation                                                                                                 |
| `/packages`                  | keeps microservice packages                                                                                            |
| `.editorconfig`              | EditorConfig configuration, to keep settings across different editors                                                  |
| `.env`                       | environment variables file (not included in repository by default)                                                     |
| `.gitlab-ci.yml`             | GitLab CI configuration                                                                                                |
| `.prettierrc`                | Prettier configuration                                                                                                 |
| `docker-compose.yml`         | docker-compose file for production-like environment                                                                    |
| `docker-compose.dev.yml`     | docker-compose configuration for development environment. Overrides parts of the configuration of `docker-compose.yml` |
| `lerna.json`                 | Lerna configuration                                                                                                    |
| `package.json`               | npm project metadata and dependencies                                                                                  |
