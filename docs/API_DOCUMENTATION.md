# API Documentation

Below is a basic description on what endpoints currently exist. Wait for the full specification to learn how to use them (or request to join our Postman Collection via mail or look into the code).

[[_TOC_]]

## Authentication

Accessing endpoints of the API requires no authentication. However, all endpoints starting with `/me` require a JWT bearer token, set in the header.

Customer should be able to use this service without registration, which also means, that the database doesn't store user data. A customer is session-based and gets assigned a JWT token, which holds all necessary customer information. The token must be token stored by the client and currently expires after four hours.

## Category Service

Manages Categories, which can be assigned to products.

| Method | Endpoint                  | Description         |
| ------ | ------------------------- | ------------------- |
| get    | `/categories`             | get all categories  |
| post   | `/categories`             | create new category |
| get    | `/categories/:categoryId` | get category        |
| patch  | `/categories/:categoryId` | patch category      |
| delete | `/categories/:categoryId` | delete category     |

## Product Service

Manages products.

| Method | Endpoint               | Description        |
| ------ | ---------------------- | ------------------ |
| get    | `/products`            | get all products   |
| post   | `/products`            | create new product |
| get    | `/products/:productId` | get product        |
| patch  | `/products/:productId` | patch product      |
| delete | `/products/:productId` | delete product     |

`GET /products` supports the following queries:

- `?categoryId` get products of category

## Table Service

Manages tables and customers, which a registered to a table.

| Method | Endpoint                     | Description                |
| ------ | ---------------------------- | -------------------------- |
| get    | `/tables`                    | get all tables             |
| post   | `/tables`                    | create new table           |
| get    | `/tables/:tableId`           | get table                  |
| patch  | `/tables/:tableId`           | patch table                |
| delete | `/tables/:tableId`           | delete table               |
| post   | `/tables/:tableId/session`   | register customer at table |
| delete | `/tables/:tableId/session`   | remove customer from table |
| delete | `/tables/:tableId/customers` | get customers of table     |

## Cart Service

Manages carts and a cart of a customer. A customer can have only one cart.

| Method | Endpoint              | Description                           |
| ------ | --------------------- | ------------------------------------- |
| get    | `/carts`              | get all carts                         |
| post   | `/carts`              | create new table                      |
| get    | `/carts/:cartId`      | get cart                              |
| patch  | `/carts/:cartId`      | patch cart                            |
| delete | `/carts/:cartId`      | delete cart                           |
| get    | `/me/cart`            | get cart of current customer          |
| post   | `/me/cart`            | create cart item for current customer |
| patch  | `/me/cart/:productId` | update cart item for current customer |
| delete | `/me/cart/:productId` | delete cart items for current         |

## Order Service

Manages orders and orders of a customer.

| Method | Endpoint     | Description                                |
| ------ | ------------ | ------------------------------------------ |
| get    | `/orders`    | get all orders                             |
| post   | `/orders`    | create new order                           |
| get    | `/me/orders` | get orders of current customer             |
| post   | `/me/orders` | creates order from current customer's cart |

`GET /orders` supports the following queries:

- `?tableId` get orders by tableId
- `?customerId` get orders by customerId
- `?isPaid={false|true}` get orders by payment status **(Unimplemented)**

## Payment Service

Manages payments made by a customer.

| Method | Endpoint       | Description                                          |
| ------ | -------------- | ---------------------------------------------------- |
| post   | `/me/payments` | creates payment for current customer's unpaid orders |
