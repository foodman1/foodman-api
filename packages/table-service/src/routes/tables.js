import express from "express";

import AuthenticationMiddleware from "../middlewares/authentication";

import TablesController from "../controllers/tables";

const router = express.Router();

router.get("/", TablesController.getTables);
router.post("/", TablesController.createTable);
router.get("/:tableId", TablesController.getTable);
router.patch("/:tableId", TablesController.patchTable);
router.delete("/:tableId", TablesController.deleteTable);

router.get("/:tableId/customers", TablesController.getCustomersOfTable);
router.post("/:tableId/session", TablesController.addCustomerToTable);
router.delete("/:tableId/session", AuthenticationMiddleware.authenticate, TablesController.deleteCustomerFromTable);

export default router;
