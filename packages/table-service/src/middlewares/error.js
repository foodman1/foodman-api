import mongoose from "mongoose";

exports.handleDatabaseError = (databaseError) => {
  return (req, res, next) => {
    if (mongoose.connection.readyState != 1) {
      const error = new Error();
      error.status = 500;
      error.message = databaseError.message || "MongoError";
      error.description = databaseError.description || "Database unavailable";
      next(error);
    }
    next();
  };
};

exports.handleUndefinedRoutes = (req, res, next) => {
  const error = new Error();
  error.status = 404;
  next(error);
};

exports.handleError = (error, req, res, next) => {
  const errorMessages = {
    400: "Bad Request",
    401: "Unauthorized Request",
    403: "Forbidden",
    404: "Not Found",
    405: "Method Not Allowed",
    500: "Internal Server Error",
    501: "Unimplemented",
  };

  error.status = error.status || 500;
  error.message = error.message || errorMessages[error.status] || "Undefined Error";
  error.description = error.description;
  error.errors = error.errors;

  if (error.status === 500) {
    console.error(error);
  }

  res.status(error.status);
  res.json(error);
};
