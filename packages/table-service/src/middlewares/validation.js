import * as OpenApiValidator from "express-openapi-validator";

import path from "path";

exports.validateRequestsAndResponses = OpenApiValidator.middleware({
  apiSpec: path.join(__dirname, "./../config/openapi.yml"),
  validateRequests: true,
  validateResponses: true,
  validateApiSpec: true,
});
