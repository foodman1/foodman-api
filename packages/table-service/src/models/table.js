import mongoose from "mongoose";
import Customer from "./customer";

const tableSchema = mongoose.Schema({
  _id: mongoose.Schema.ObjectId,
  number: { type: String, required: true },
  customers: { type: [Customer.schema], required: true },
  __v: { type: Number, select: false },
});

tableSchema.set("toJSON", {
  virtuals: true,
  transform: function (doc, ret) {
    delete ret._id;
  },
});

tableSchema.set("toObject", {
  virtuals: true,
  transform: function (doc, ret) {
    delete ret._id;
  },
});

export default mongoose.model("Table", tableSchema);
