import mongoose from "mongoose";

const customerSchema = mongoose.Schema({
  _id: mongoose.Schema.ObjectId,
  name: { type: String, required: true },
  cartId: { type: String, required: true },
  __v: { type: Number, select: false },
});

customerSchema.set("toJSON", {
  virtuals: true,
  transform: function (doc, ret) {
    delete ret._id;
  },
});

customerSchema.set("toObject", {
  virtuals: true,
  transform: function (doc, ret) {
    delete ret._id;
  },
});

export default mongoose.model("Customer", customerSchema);
