import mongoose from "mongoose";
import jwt from "jsonwebtoken";
import got from "got";

import Table from "../models/table";
import Customer from "../models/customer";

exports.getTables = async (req, res, next) => {
  try {
    const tables = await Table.find().exec();
    res.status(200).json(tables);
  } catch (e) {
    const error = new Error();
    error.status = 500;
    error.description = e;
    next(error);
  }
};

exports.createTable = async (req, res, next) => {
  const table = new Table({
    _id: new mongoose.Types.ObjectId(),
    number: req.body.number,
  });

  try {
    await table.save();
    res.status(201).json(table);
  } catch (e) {
    const error = new Error();
    error.status = 500;
    error.description = e;
    next(error);
  }
};

exports.getTable = async (req, res, next) => {
  try {
    const table = await Table.findById(req.params.tableId).exec();
    if (!table) {
      const error = new Error();
      error.status = 404;
      error.description = "Table does not exist";
      return next(error);
    }
    res.status(200).json(table);
  } catch (e) {
    const error = new Error();
    error.status = 500;
    error.description = e;
    next(error);
  }
};

exports.patchTable = async (req, res, next) => {
  const patchQuery = {};

  for (const key in req.body) {
    patchQuery[key] = req.body[key];
  }

  try {
    const table = await Table.findByIdAndUpdate(
      { _id: req.params.tableId },
      { $set: patchQuery },
      { new: true }
    ).exec();
    res.status(200).json(table);
  } catch (e) {
    const error = new Error();
    error.status = 500;
    error.description = e;
    next(error);
  }
};

exports.deleteTable = async (req, res, next) => {
  try {
    await Table.deleteOne({ _id: req.params.tableId }).exec();
    res.status(204).send();
  } catch (e) {
    const error = new Error();
    error.status = 500;
    error.description = e;
    next(error);
  }
};

exports.addCustomerToTable = async (req, res, next) => {
  const customer = new Customer({
    _id: new mongoose.Types.ObjectId(),
    name: req.body.customerName,
  });

  // create cart for customer
  try {
    const CARTS_URL = `http://${process.env.CART_SERVICE_NAME}:${process.env.CART_SERVICE_PORT}/carts`;
    const cart = await got.post(CARTS_URL).json();
    customer.cartId = cart.id;
  } catch (e) {
    const error = new Error();
    error.status = 500;
    error.description = e;
    next(error);
  }

  const token = jwt.sign(
    { customerId: customer._id, tableId: req.params.tableId, cartId: customer.cartId },
    process.env.JWT_PRIVATE_KEY,
    {
      expiresIn: "4h",
    }
  );

  try {
    // NOTE: findOneAndUpdate overwrites the id with null, updateOne is used instead
    const udpateResponse = await Table.updateOne(
      { _id: req.params.tableId },
      {
        $push: {
          customers: customer,
        },
      },
      { new: true }
    ).exec();

    // check number of documents, which matched query filter
    if (udpateResponse.n === 0) {
      const error = new Error();
      error.status = 404;
      error.description = "Table does not exist";
      return next(error);
    }
    return res.status(200).json({
      token: token,
    });
  } catch (e) {
    const error = new Error();
    error.status = 500;
    error.description = e;
    next(error);
  }
};

exports.deleteCustomerFromTable = async (req, res, next) => {
  try {
    const table = await Table.findOneAndUpdate(
      { _id: req.params.tableId },
      {
        $pull: {
          customers: { _id: req.customerId },
        },
      },
      { new: true }
    ).exec();
    if (!table) {
      const error = new Error();
      error.status = 404;
      error.description = "Table does not exist";
      return next(error);
    }

    // delete cart of custoemr
    const CARTS_URL = `http://${process.env.CART_SERVICE_NAME}:${process.env.CART_SERVICE_PORT}/carts/${req.cartId}`;
    await got.delete(CARTS_URL);

    res.status(204).json(table);
  } catch (e) {
    const error = new Error();
    error.status = 500;
    error.description = e;
    next(error);
  }
};

exports.getCustomersOfTable = async (req, res, next) => {
  try {
    const table = await Table.findOne({
      _id: req.params.tableId,
    }).exec();
    if (!table) {
      const error = new Error();
      error.status = 404;
      error.description = "Table does not exist";
      return next(error);
    }
    res.status(200).json(table.customers);
  } catch (e) {
    const error = new Error();
    error.status = 500;
    error.description = e;
    next(error);
  }
};
