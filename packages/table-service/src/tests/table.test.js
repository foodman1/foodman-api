import mongoose from "mongoose";
import { MongoMemoryServer } from "mongodb-memory-server";
import supertest from "supertest";

import got from "got";
jest.mock("got");

import app from "../app";

let mongoMemoryServer;
const request = supertest(app);

beforeAll(async () => {
  mongoMemoryServer = new MongoMemoryServer();
  const mongoDbUri = await mongoMemoryServer.getUri();
  await mongoose.connect(mongoDbUri, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false });
});

afterAll(async () => {
  await mongoose.disconnect();
  await mongoMemoryServer.stop();
});

describe("Table Routes", () => {
  const table = {
    number: "42",
  };
  const customer = {
    name: "John Doe",
  };
  const cartMockData = {
    id: "abc",
    items: [],
    createdAt: new Date(),
  };
  let token; // JWT token

  it("should get an empty array", async () => {
    const res = await request.get("/tables").send();

    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual([]);
  });

  it("should create a single table", async () => {
    const res = await request.post("/tables").send(table);

    expect(res.statusCode).toEqual(201);
    expect(res.body).toHaveProperty("id");
    expect(res.body).toHaveProperty("number", table.number);
    expect(res.body).toHaveProperty("customers", []);

    table.id = res.body.id;
    table.customers = res.body.customers;
  });

  it("should get an array with one table", async () => {
    const res = await request.get("/tables").send();

    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual([table]);
  });

  it("should update a single table", async () => {
    const updatedNumber = "1337";

    const res = await request.patch(`/tables/${table.id}`).send({
      number: updatedNumber,
    });

    table.number = updatedNumber;

    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual(table);
  });

  it("should get a single table", async () => {
    const res = await request.get(`/tables/${table.id}`).send();

    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual(table);
  });

  it("should add a customer to the table", async () => {
    got.post.mockReturnValue({ json: jest.fn().mockReturnValue(cartMockData) });

    const res = await request.post(`/tables/${table.id}/session`).send({
      customerName: customer.name,
    });

    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveProperty("token");

    token = res.body.token;

    // adding a customer should create a new cart
    expect(got.post).toHaveBeenCalledWith(
      `http://${process.env.CART_SERVICE_NAME}:${process.env.CART_SERVICE_PORT}/carts`
    );
    expect(got.post).toHaveBeenCalled();
    expect(got.post().json).toHaveBeenCalled();
    expect(got.post().json).toHaveReturnedWith(cartMockData);

    customer.cartId = cartMockData.id;
  });

  it("should get all customers of a table", async () => {
    const res = await request.get(`/tables/${table.id}/customers`).send();

    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveLength(1);
    expect(res.body[0]).toHaveProperty("id");
    expect(res.body[0]).toHaveProperty("cartId", customer.cartId);
    expect(res.body[0]).toHaveProperty("name", customer.name);

    customer.id = res.body[0].id;
  });

  it("should remove an authenticated customer from a table", async () => {
    got.delete = jest.fn();

    const res = await request.delete(`/tables/${table.id}/session`).set("Authorization", `Bearer ${token}`).send();

    expect(res.statusCode).toEqual(204);

    // removing customer should delete cart
    expect(got.delete).toHaveBeenCalledWith(
      `http://${process.env.CART_SERVICE_NAME}:${process.env.CART_SERVICE_PORT}/carts/${customer.cartId}`
    );
  });

  it("should delete a single table", async () => {
    const res = await request.delete(`/tables/${table.id}`).send();

    expect(res.statusCode).toEqual(204);
    expect(res.body).toEqual({});
  });
});
