import express from "express";

import CartsController from "../controllers/carts";

const router = express.Router();

router.get("/", CartsController.getCarts);
router.post("/", CartsController.createCart);
router.get("/:cartId", CartsController.getCart);
router.patch("/:cartId", CartsController.updateCart);
router.delete("/:cartId", CartsController.deleteCart);

export default router;
