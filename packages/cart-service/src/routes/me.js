import express from "express";

import AuthenticationMiddleware from "../middlewares/authentication";

import CartController from "../controllers/carts";

const router = express.Router();

router.get("/", AuthenticationMiddleware.authenticate, CartController.getCartOfAuthenticatedCustomer);
router.post("/", AuthenticationMiddleware.authenticate, CartController.createCartItemForAuthenticatedCustomer);
router.patch(
  "/:productId",
  AuthenticationMiddleware.authenticate,
  CartController.updateCartItemOfAuthenticatedCustomer
);
router.delete(
  "/:productId",
  AuthenticationMiddleware.authenticate,
  CartController.deleteCartItemOfAuthenticatedCustomer
);

export default router;
