import express from "express";
import morgan from "morgan";

import HeaderMiddleware from "./middlewares/header";
import ErrorMiddleware from "./middlewares/error";
import ValidationMiddleware from "./middlewares/validation";

import cartRoutes from "./routes/carts";
import meRoutes from "./routes/me";

const app = express();

if (process.env.NODE_ENV !== "test") {
  app.use(morgan("dev"));
}

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use(HeaderMiddleware.setAccessHeaders);

// app.use(ValidationMiddleware.validateRequestsAndResponses);

app.use("/carts", cartRoutes);
app.use("/me/cart", meRoutes);

app.use(ErrorMiddleware.handleUndefinedRoutes);
app.use(ErrorMiddleware.handleError);

export default app;
