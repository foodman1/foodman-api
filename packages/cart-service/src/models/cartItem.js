import mongoose from "mongoose";

import Product from "./product";

const cartItemSchema = mongoose.Schema(
  {
    product: { type: mongoose.Schema.Types.ObjectId, ref: "Product" },
    customerId: { type: String, required: true },
    quantity: { type: Number, required: true },
    __v: { type: Number, select: false },
  },
  { _id: false }
);

export default mongoose.model("CartItem", cartItemSchema);
