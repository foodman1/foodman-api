import mongoose from "mongoose";

const productSchema = mongoose.Schema({
  _id: mongoose.Schema.ObjectId,
  categoryId: { type: String, required: true },
  description: { type: String, default: "" },
  image: { type: String, required: true },
  name: { type: String, required: true },
  price: { type: Number, required: true },
  __v: { type: Number, select: false },
});

productSchema.set("toJSON", {
  virtuals: true,
  transform: function (doc, ret) {
    delete ret._id;
  },
});

productSchema.set("toObject", {
  virtuals: true,
  transform: function (doc, ret) {
    delete ret._id;
  },
});

export default mongoose.model("Product", productSchema);
