import mongoose from "mongoose";

import CartItem from "./cartItem";

const cartSchema = mongoose.Schema({
  _id: mongoose.Schema.ObjectId,
  items: { type: [CartItem.schema] },
  createdAt: { type: Date, required: true },
  __v: { type: Number, select: false },
});

cartSchema.set("toJSON", {
  virtuals: true,
  transform: function (doc, ret) {
    delete ret._id;
  },
});

cartSchema.set("toObject", {
  virtuals: true,
  transform: function (doc, ret) {
    delete ret._id;
  },
});

export default mongoose.model("Cart", cartSchema);
