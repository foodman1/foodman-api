import mongoose from "mongoose";

import Cart from "../models/cart";
import CartItem from "../models/cartItem";

exports.getCarts = async (req, res, next) => {
  try {
    const carts = await Cart.find().populate("items.product").exec();
    res.status(200).json(carts);
  } catch (e) {
    const error = new Error();
    error.status = 500;
    error.description = e;
    next(error);
  }
};

exports.createCart = async (req, res, next) => {
  const cart = new Cart({
    _id: new mongoose.Types.ObjectId(),
    items: req.body.items,
    createdAt: new Date(),
  });

  try {
    await cart.save();
    res.status(201).json(cart);
  } catch (e) {
    const error = new Error();
    error.status = 500;
    error.description = e;
    next(error);
  }
};

exports.getCart = async (req, res, next) => {
  try {
    const cart = await Cart.findById(req.params.cartId).populate("product").exec();
    if (!cart) {
      const error = new Error();
      error.status = 404;
      error.description = "Cart does not exist";
      return next(error);
    }
    res.status(200).json(cart);
  } catch (e) {
    const error = new Error();
    error.status = 500;
    error.description = e;
    next(error);
  }
};

exports.updateCart = async (req, res, next) => {
  const patchQuery = {};

  for (const key in req.body) {
    patchQuery[key] = req.body[key];
  }

  try {
    const cart = await Cart.findByIdAndUpdate({ _id: req.params.cartId }, { $set: patchQuery }, { new: true }).exec();
    if (!cart) {
      const error = new Error();
      error.status = 404;
      error.description = "Cart does not exist";
      return next(error);
    }
    res.status(200).json(cart);
  } catch (e) {
    const error = new Error();
    error.status = 500;
    error.description = e;
    next(error);
  }
};

exports.deleteCart = async (req, res, next) => {
  try {
    await Cart.deleteOne({ _id: req.params.cartId }).exec();
    res.status(204).send();
  } catch (e) {
    const error = new Error();
    error.status = 500;
    error.description = e;
    next(error);
  }
};

exports.getCartOfAuthenticatedCustomer = async (req, res, next) => {
  try {
    const cart = await Cart.findById(req.cartId).populate("items.product").exec();
    if (!cart) {
      const error = new Error();
      error.status = 404;
      error.description = "Cart does not exist";
      return next(error);
    }
    res.status(200).json(cart);
  } catch (e) {
    const error = new Error();
    error.status = 500;
    error.description = e;
    next(error);
  }
};

exports.createCartItemForAuthenticatedCustomer = async (req, res, next) => {
  const cartItem = new CartItem({
    product: req.body.productId,
    quantity: req.body.quantity,
  });

  try {
    let cart;
    // check if cart item already exists for authenticated user
    const isItemAlreadyInCart = await Cart.exists({
      _id: req.cartId,
      "items.product": req.body.productId,
    });

    if (isItemAlreadyInCart) {
      cart = await Cart.findOneAndUpdate(
        { _id: req.cartId, "items.product": req.body.productId },
        {
          $inc: { "items.$.quantity": req.body.quantity },
        },
        { new: true }
      ).exec();
    } else {
      cart = await Cart.findOneAndUpdate(
        { _id: req.cartId },
        {
          $push: {
            items: cartItem,
          },
        },
        { new: true }
      ).exec();
    }
    if (!cart) {
      const error = new Error();
      error.status = 404;
      error.description = "Product does not exist in cart";
      return next(error);
    }
    return res.status(201).send(cart);
  } catch (e) {
    const error = new Error();
    error.status = 500;
    error.description = e;
    next(error);
  }
};

exports.updateCartItemOfAuthenticatedCustomer = async (req, res, next) => {
  const quantity = req.body.quantity;

  try {
    const cart = await Cart.findOneAndUpdate(
      {
        _id: req.cartId,
        "items.product": req.params.productId,
      },
      {
        $set: { "items.$.quantity": quantity },
      },
      { new: true }
    ).exec();
    if (!cart) {
      const error = new Error();
      error.status = 404;
      error.description = "Product does not exist in cart";
      return next(error);
    }
    res.status(200).send(cart);
  } catch (e) {
    const error = new Error();
    error.status = 500;
    error.description = e;
    next(error);
  }
};

exports.deleteCartItemOfAuthenticatedCustomer = async (req, res, next) => {
  try {
    const cart = await Cart.updateOne(
      { _id: req.cartId },
      {
        $pull: {
          items: { product: req.params.productId },
        },
      }
    ).exec();
    res.status(204).send();
  } catch (e) {
    const error = new Error();
    error.status = 500;
    error.description = e;
    next(error);
  }
};
