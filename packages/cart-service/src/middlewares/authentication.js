import jwt from "jsonwebtoken";

exports.authenticate = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(" ")[1];
    const decodedToken = jwt.verify(token, process.env.JWT_PRIVATE_KEY);
    req.customerId = decodedToken.customerId;
    req.cartId = decodedToken.cartId;
    req.tableId = decodedToken.tableId;
    next();
  } catch (e) {
    return res.status(401).json({ error: e });
  }
};
