import mongoose from "mongoose";
import { MongoMemoryServer } from "mongodb-memory-server";
import supertest from "supertest";

import AuthenticationMiddleware from "../middlewares/authentication";
jest.mock("../middlewares/authentication");

import app from "../app";

let mongoMemoryServer;
const request = supertest(app);

beforeAll(async () => {
  mongoMemoryServer = new MongoMemoryServer();
  const mongoDbUri = await mongoMemoryServer.getUri();
  await mongoose.connect(mongoDbUri, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false });
});

afterAll(async () => {
  await mongoose.disconnect();
  await mongoMemoryServer.stop();
});

describe("Cart Routes", () => {
  const cartItem = {
    product: new mongoose.Types.ObjectId().toString(), // needs to be the same format as an object id
    quantity: 42,
  };

  const cart = {
    items: [],
  };

  it("should get an empty array", async () => {
    const res = await request.get("/carts").send();

    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual([]);
  });

  it("should create a single cart with empty items", async () => {
    const res = await request.post("/carts").send({ items: cart.items });

    expect(res.statusCode).toEqual(201);
    expect(res.body).toHaveProperty("id");
    expect(res.body).toHaveProperty("items", cart.items);
    expect(res.body).toHaveProperty("createdAt");

    cart.id = res.body.id;
    cart.createdAt = res.body.createdAt;
  });

  it("should get an array with one cart", async () => {
    const res = await request.get("/carts").send();

    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual([cart]);
  });

  it("should update a single cart", async () => {
    const res = await request.patch(`/carts/${cart.id}`).send({
      items: [cartItem],
    });

    cart.items.push(cartItem);

    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual(cart);
  });

  it("should get a single cart", async () => {
    const res = await request.get(`/carts/${cart.id}`).send();

    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual(cart);
  });

  it("should delete a single cart", async () => {
    const res = await request.delete(`/carts/${cart.id}`).send();

    expect(res.statusCode).toEqual(204);
    expect(res.body).toEqual({});
  });
});

describe("Me Routes", () => {
  const customerId = new mongoose.Types.ObjectId().toString();
  const tableId = new mongoose.Types.ObjectId().toString();

  const cart = {};
  const cartItem = {
    product: new mongoose.Types.ObjectId().toString(), // needs to be the same format as an object id
    quantity: 42,
  };

  // the cart service is unable to create a jwt token
  // therefore the authentication middleware is mocked
  AuthenticationMiddleware.authenticate.mockImplementation((req, res, next) => {
    req.customerId = customerId;
    req.cartId = cart.id;
    req.tableId = tableId;
    next();
  });

  it("should create a single cart with empty items for the customer", async () => {
    // create a new cart the customer can use
    const res = await request.post("/carts").send();

    expect(res.statusCode).toEqual(201);
    expect(res.body).toHaveProperty("id");
    expect(res.body).toHaveProperty("items", []);
    expect(res.body).toHaveProperty("createdAt");

    cart.id = res.body.id;
    cart.items = res.body.items;
    cart.createdAt = res.body.createdAt;
  });

  it("should return the cart of the authenticated customer", async () => {
    const res = await request.get(`/me/cart`).send();

    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual(cart);
  });

  it("should create an cart item in the customer's cart", async () => {
    const res = await request.post(`/me/cart`).send({
      productId: cartItem.product,
      quantity: cartItem.quantity,
    });

    cart.items.push(cartItem);

    expect(res.statusCode).toEqual(201);
    expect(res.body).toEqual(cart);
  });

  it("should increment the quantity if product is already in cart", async () => {
    const quantityIncrement = 2;
    const res = await request.post(`/me/cart`).send({
      productId: cartItem.product,
      quantity: quantityIncrement,
    });

    cart.items[0].quantity = cartItem.quantity + quantityIncrement;

    expect(res.statusCode).toEqual(201);
    expect(res.body).toEqual(cart);
  });

  it("should update the quantity of an existing product", async () => {
    const updatedQuantity = 2;
    const res = await request.patch(`/me/cart/${cartItem.product}`).send({
      quantity: updatedQuantity,
    });

    cart.items[0].quantity = updatedQuantity;

    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual(cart);
  });

  it("should update the quantity of an existing product", async () => {
    const updatedQuantity = 2;
    const res = await request.patch(`/me/cart/${cartItem.product}`).send({
      quantity: updatedQuantity,
    });

    cart.items[0].quantity = updatedQuantity;

    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual(cart);
  });

  it("should delete an cart item from the customer's cart", async () => {
    const res = await request.delete(`/me/cart/${cartItem.product}`).send();

    cart.items = [];

    expect(res.statusCode).toEqual(204);
    expect(res.body).toEqual({});
  });
});
