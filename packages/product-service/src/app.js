import express from "express";
import morgan from "morgan";

import HeaderMiddleware from "./middlewares/header";
import ErrorMiddleware from "./middlewares/error";
import ValidationMiddleware from "./middlewares/validation";

import productsRouter from "./routes/products";

const app = express();

if (process.env.NODE_ENV !== "test") {
  app.use(morgan("dev"));
}

app.use("/images", express.static("./src/images"));

app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use(HeaderMiddleware.setAccessHeaders);

app.use(ValidationMiddleware.validateRequestsAndResponses);

app.use("/products", productsRouter);

app.use(ErrorMiddleware.handleUndefinedRoutes);
app.use(ErrorMiddleware.handleError);

export default app;
