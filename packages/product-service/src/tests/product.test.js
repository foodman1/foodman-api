import fs from "fs";
import path from "path";
import mongoose from "mongoose";
import { MongoMemoryServer } from "mongodb-memory-server";
import supertest from "supertest";

import app from "../app";

let mongoMemoryServer;
const request = supertest(app);

beforeAll(async () => {
  mongoMemoryServer = new MongoMemoryServer();
  const mongoDbUri = await mongoMemoryServer.getUri();
  await mongoose.connect(mongoDbUri, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false });
});

afterAll(async () => {
  await mongoose.disconnect();
  await mongoMemoryServer.stop();
});

describe("Product Routes", () => {
  const product = {
    categoryId: "123abc",
    name: "Apple",
    description: "Delicious!",
    price: 12.5,
  };
  const imageName = "rammar.png";
  const image = path.resolve(__dirname, `./images/${imageName}`);

  it("should get an empty array", async () => {
    const res = await request.get("/products").send();

    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual([]);
  });

  it("should create a single product", async () => {
    var res = await request
      .post("/products")
      .field("categoryId", product.categoryId)
      .field("name", product.name)
      .field("description", product.description)
      .field("price", product.price)
      .attach("image", image);

    expect(res.statusCode).toEqual(201);
    expect(res.body).toHaveProperty("id");
    expect(res.body).toHaveProperty("categoryId", product.categoryId);
    expect(res.body).toHaveProperty("name", product.name);
    expect(res.body).toHaveProperty("description", product.description);
    expect(res.body.image).toMatch(new RegExp("/images/[\\d]{13}_" + imageName));
    expect(res.body).toHaveProperty("price", product.price);

    product.id = res.body.id;
    product.image = res.body.image;

    res = await request.get(product.image).send();

    // check if image was created
    expect(res.statusCode).toEqual(200);
    expect(res.get("content-type")).toMatch(/image\/(png|jpeg)/);
    expect(fs.existsSync("./src" + product.image)).toBe(true);
  });

  it("should get an array with one product", async () => {
    const res = await request.get("/products").send();

    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual([product]);
  });

  it("should update a single product", async () => {
    const updatedDescription = "Magnificient!";

    const res = await request.patch(`/products/${product.id}`).send({
      description: updatedDescription,
    });

    product.description = updatedDescription;

    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual(product);
  });

  it("should get a single product", async () => {
    const res = await request.get(`/products/${product.id}`).send();

    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual(product);
  });

  it("should delete a single product", async () => {
    var res = await request.delete(`/products/${product.id}`).send();

    expect(res.statusCode).toEqual(204);
    expect(res.body).toEqual({});

    res = await request.get(product.image).send();

    // check if image was deleted
    expect(res.statusCode).toEqual(404);
    expect(fs.existsSync("./src" + product.image)).toBe(false);
  });
});
