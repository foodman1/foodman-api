import express from "express";

import ImageMiddleware from "../middlewares/image";

import ProductController from "../controllers/product";

const router = express.Router();

router.get("/", ProductController.getProducts);
router.post("/", ImageMiddleware.uploadImage("image"), ProductController.createProduct);
router.get("/:productId", ProductController.getProduct);
router.patch("/:productId", ProductController.patchProduct);
router.delete("/:productId", ProductController.deleteProduct);

export default router;
