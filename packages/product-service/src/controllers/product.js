import fs from "fs";
import mongoose from "mongoose";

import Product from "../models/product";

exports.getProducts = async (req, res, next) => {
  let query = {};
  if (req.query.categoryId) query.categoryId = req.query.categoryId;

  try {
    const products = await Product.find(query).exec();
    res.status(200).json(products);
  } catch (e) {
    res.status(500).json({ error: e });
  }
};

exports.createProduct = async (req, res, next) => {
  const product = new Product({
    _id: new mongoose.Types.ObjectId(),
    categoryId: req.body.categoryId,
    description: req.body.description,
    image: "/images/" + req.file.filename,
    name: req.body.name,
    price: req.body.price,
  });

  try {
    await product.save();
    res.status(201).json(product);
  } catch (e) {
    const error = new Error();
    error.status = 500;
    error.description = e;
    next(error);
  }
};

exports.getProduct = async (req, res, next) => {
  try {
    const product = await Product.findById(req.params.productId).exec();
    if (!product) {
      const error = new Error();
      error.status = 404;
      error.description = "Product does not exist";
      return next(error);
    }
    res.status(200).json(product);
  } catch (e) {
    const error = new Error();
    error.status = 500;
    error.description = e;
    next(error);
  }
};

exports.patchProduct = async (req, res, next) => {
  const patchQuery = {};

  for (const key in req.body) {
    patchQuery[key] = req.body[key];
  }

  try {
    const product = await Product.findOneAndUpdate(
      { _id: req.params.productId },
      { $set: patchQuery },
      { new: true }
    ).exec();
    if (!product) {
      const error = new Error();
      error.status = 404;
      error.description = "Product does not exist";
      return next(error);
    }
    res.status(200).json(product);
  } catch (e) {
    const error = new Error();
    error.status = 500;
    error.description = e;
    next(error);
  }
};

exports.deleteProduct = async (req, res, next) => {
  try {
    const product = await Product.findOneAndDelete({ _id: req.params.productId }).exec();
    if (!product) {
      const error = new Error();
      error.status = 404;
      error.description = "Product does not exist";
      return next(error);
    }
    // delete product image
    fs.unlinkSync("src" + product.image);
    res.status(204).send();
  } catch (e) {
    const error = new Error();
    console.error(e);
    error.status = 500;
    error.description = e;
    next(error);
  }
};
