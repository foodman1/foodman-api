import mongoose from "mongoose";
import { MongoMemoryServer } from "mongodb-memory-server";
import supertest from "supertest";

import got from "got";
jest.mock("got");

import AuthenticationMiddleware from "../middlewares/authentication";
jest.mock("../middlewares/authentication");

import app from "../app";

let mongoMemoryServer;
const request = supertest(app);

beforeAll(async () => {
  mongoMemoryServer = new MongoMemoryServer();
  const mongoDbUri = await mongoMemoryServer.getUri();
  await mongoose.connect(mongoDbUri, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false });
});

afterAll(async () => {
  await mongoose.disconnect();
  await mongoMemoryServer.stop();
});

describe("Order Routes", () => {
  const orderItem = {
    product: new mongoose.Types.ObjectId().toString(),
    quantity: 42,
  };

  const order = {
    items: [],
    tableId: new mongoose.Types.ObjectId().toString(),
    customerId: new mongoose.Types.ObjectId().toString(),
  };

  it("should get an empty array", async () => {
    const res = await request.get("/orders").send();

    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual([]);
  });

  // it("should NOT create a single order with empty items", async () => {
  //   const res = await request.post("/orders").send(order);

  //   expect(res.statusCode).toEqual(400);
  //   expect(res.body).toHaveProperty("id");
  //   expect(res.body).toHaveProperty("items", order.items);
  //   expect(res.body).toHaveProperty("createdAt");
  //   expect(res.body).toHaveProperty("paidAt");

  //   order.id = res.body.id;
  //   order.createdAt = res.body.createdAt;
  // });

  it("should create a single order with items", async () => {
    order.items.push(orderItem);
    const res = await request.post("/orders").send(order);

    expect(res.statusCode).toEqual(201);
    expect(res.body).toHaveProperty("id");
    expect(res.body).toHaveProperty("items", order.items);
    expect(res.body).toHaveProperty("createdAt");
    expect(res.body).toHaveProperty("paidAt", null);

    order.id = res.body.id;
    order.createdAt = res.body.createdAt;
    order.paidAt = res.body.paidAt;
    // TODO: mongoose.population will not work since the product is unknown
    // Why does it work in the cart test?
    order.items[0].product = null;
  });

  it("should get an array with one order", async () => {
    const res = await request.get("/orders").send();

    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual([order]);
  });
});

describe("Me Routes", () => {
  const customerId = new mongoose.Types.ObjectId().toString();
  const tableId = new mongoose.Types.ObjectId().toString();
  const productId = new mongoose.Types.ObjectId().toString();
  const cartId = new mongoose.Types.ObjectId().toString();

  const mockedCartItem = {
    product: productId, // needs to be the same format as an object id
    quantity: 42,
  };
  const mockedCart = {
    id: cartId,
    items: [mockedCartItem],
    createdAt: new Date(),
  };
  const mockedCartAfterOrder = {
    id: cartId,
    items: [],
    createdAt: new Date(),
  };

  const order = {};

  // the order service is unable to create a jwt token
  // therefore the authentication middleware is mocked
  AuthenticationMiddleware.authenticate.mockImplementation((req, res, next) => {
    req.customerId = customerId;
    req.cartId = mockedCart.id;
    req.tableId = tableId;
    next();
  });

  it("should create a single order for the customer", async () => {
    got.get.mockReturnValue({ json: jest.fn().mockReturnValue(mockedCart) });
    got.patch.mockReturnValue({ json: jest.fn().mockReturnValue(mockedCartAfterOrder) });

    const res = await request.post("/me/orders").send();

    // creating an order should get the current cart of the customer
    expect(got.get).toHaveBeenCalledWith(
      `http://${process.env.CART_SERVICE_NAME}:${process.env.CART_SERVICE_PORT}/carts/${mockedCart.id}`
    );
    expect(got.get).toHaveBeenCalled();
    expect(got.get().json).toHaveBeenCalled();
    expect(got.get().json).toHaveReturnedWith(mockedCart);

    order.items = mockedCart.items;

    // creating an order should empty the current cart items of the customer
    expect(got.patch).toHaveBeenCalledWith(
      `http://${process.env.CART_SERVICE_NAME}:${process.env.CART_SERVICE_PORT}/carts/${mockedCart.id}`,
      {
        json: {
          items: [],
        },
      }
    );
    expect(got.patch).toHaveBeenCalled();
    expect(got.patch().json).toHaveBeenCalled();
    expect(got.patch().json).toHaveReturnedWith(mockedCartAfterOrder);

    // the created order should return
    expect(res.statusCode).toEqual(201);
    expect(res.body).toHaveProperty("id");
    expect(res.body).toHaveProperty("items", order.items);
    expect(res.body).toHaveProperty("tableId", tableId);
    expect(res.body).toHaveProperty("customerId", customerId);
    expect(res.body).toHaveProperty("createdAt");
    expect(res.body).toHaveProperty("paidAt", null);

    order.id = res.body.id;
    order.items = res.body.items;
    order.tableId = res.body.tableId;
    order.customerId = res.body.customerId;
    order.createdAt = res.body.createdAt;
    order.paidAt = res.body.paidAt;

    // TODO: mongoose.population will not work since the product is unknown
    // Why does it work in the cart test?
    order.items[0].product = null;
  });

  it("should return all orders of the authenticated customer", async () => {
    const res = await request.get(`/me/orders`).send();
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual([order]);
  });
});
