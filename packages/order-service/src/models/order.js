import mongoose from "mongoose";

import OrderItem from "./orderItem";

const orderSchema = mongoose.Schema({
  _id: mongoose.Schema.ObjectId,
  items: {
    type: [OrderItem.schema],
    validate: [(orderItems) => orderItems.length > 0, "{PATH} should be greater than 0"],
  },
  tableId: { type: mongoose.Schema.ObjectId, ref: "Table", required: true },
  customerId: { type: String, required: true },
  createdAt: { type: Date, required: true },
  paidAt: { type: Date, default: null },
  __v: { type: Number, select: false },
});

orderSchema.set("toJSON", {
  virtuals: true,
  transform: function (doc, ret) {
    delete ret._id;
  },
});

orderSchema.set("toObject", {
  virtuals: true,
  transform: function (doc, ret) {
    delete ret._id;
  },
});

export default mongoose.model("Order", orderSchema);
