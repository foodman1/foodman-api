import mongoose from "mongoose";

import Product from "./product";

const orderItemSchema = mongoose.Schema(
  {
    product: { type: mongoose.Schema.Types.ObjectId, ref: "Product" },
    quantity: { type: Number, required: true },
    __v: { type: Number, select: false },
  },
  { _id: false }
);

export default mongoose.model("OrderItem", orderItemSchema);
