import mongoose from "mongoose";
import got from "got";

import Order from "../models/order";

exports.getOrders = async (req, res, next) => {
  let query = {};
  if (req.query.tableId) query.tableId = req.query.tableId;
  if (req.query.customerId) query.customerId = req.query.customerId;
  if (req.query.isPaid === "true") query.paidAt = { $ne: null };
  if (req.query.isPaid === "false") query.paidAt = { $eq: null };

  try {
    const orders = await Order.find(query).populate("items.product").exec();
    res.status(200).json(orders);
  } catch (e) {
    const error = new Error();
    error.status = 500;
    error.description = e;
    next(error);
  }
};

exports.createOrder = async (req, res, next) => {
  const order = new Order({
    _id: new mongoose.Types.ObjectId(),
    items: req.body.items,
    tableId: req.body.tableId,
    customerId: req.body.customerId,
    createdAt: new Date(),
  });

  try {
    await order.save();
    res.status(201).json(order);
  } catch (e) {
    const error = new Error();
    error.status = 500;
    error.description = e;
    next(error);
  }
};

exports.getOrdersOfAuthenticatedCustomer = async (req, res, next) => {
  try {
    const orders = await Order.find({ customerId: req.customerId }).populate("items.product").exec();
    res.status(200).json(orders);
  } catch (e) {
    const error = new Error();
    error.status = 500;
    error.description = e;
    next(error);
  }
};

exports.createOrderForAuthenticatedCustomer = async (req, res, next) => {
  const CARTS_URL = `http://${process.env.CART_SERVICE_NAME}:${process.env.CART_SERVICE_PORT}`;

  try {
    // get cart of current customer
    const cart = await got.get(CARTS_URL + `/carts/${req.cartId}`).json();
    const order = new Order({
      _id: new mongoose.Types.ObjectId(),
      items: cart.items,
      tableId: req.tableId,
      customerId: req.customerId,
      createdAt: new Date(),
    });

    // save order
    await order.save();

    // pull items from current cart
    await got
      .patch(CARTS_URL + `/carts/${req.cartId}`, {
        json: {
          items: [],
        },
      })
      .json();
    res.status(201).json(order);
  } catch (e) {
    let error = new Error();
    error.status = 500;
    error.description = e;
    next(error);
  }
};
