import express from "express";

import AuthenticationMiddleware from "../middlewares/authentication";

import OrderController from "../controllers/order";

const router = express.Router();

router.get("/", AuthenticationMiddleware.authenticate, OrderController.getOrdersOfAuthenticatedCustomer);
router.post("/", AuthenticationMiddleware.authenticate, OrderController.createOrderForAuthenticatedCustomer);

export default router;
