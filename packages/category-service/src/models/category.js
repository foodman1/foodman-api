import mongoose from "mongoose";

/**
 * The schema for our category object. It includes some restrictions, so that the database is not filled with data,
 * that we don't want. Example: a category won't be saved if no name is given.
 */
const categorySchema = mongoose.Schema({
  _id: mongoose.Schema.ObjectId,
  description: { type: String, default: "" },
  name: { type: String, required: true },
  __v: { type: Number, select: false },
});

/**
 * MongoDB uses "_id" as a default field for the id. However, when the data is returned to the client he might expect
 * the field to be named "id". Therefore the methods "toJSON" and "toObject" additionally transform the object to
 * include "id" instead of "_id". Summary: in the DB it's "_id", in the response it's "id"
 */
categorySchema.set("toJSON", {
  virtuals: true,
  transform: function (doc, ret) {
    delete ret._id;
  },
});

categorySchema.set("toObject", {
  virtuals: true,
  transform: function (doc, ret) {
    delete ret._id;
  },
});

export default mongoose.model("Category", categorySchema);
