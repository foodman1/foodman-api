import app from "./app";

import DatabaseUtility from "./utils/database";

/**
 * Entry point of the service. First load required environment variables, which are needed to start the service
 */
const SERVICE_NAME = process.env.CATEGORY_SERVICE_NAME;
const SERVICE_URL = process.env.CATEGORY_SERVICE_URL;
const SERVICE_PORT = process.env.CATEGORY_SERVICE_PORT;

/**
 * Using the database utility to connect to the database
 */
DatabaseUtility.connectToDatabase();

/**
 * Start express application. (See app.js)
 */
app.listen(SERVICE_PORT, () => console.log(`${SERVICE_NAME} served on http://${SERVICE_URL}:${SERVICE_PORT} ...`));
