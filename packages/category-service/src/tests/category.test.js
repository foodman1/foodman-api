import mongoose from "mongoose";
import { MongoMemoryServer } from "mongodb-memory-server";
import supertest from "supertest";

import app from "../app";

/**
 * The test uses an in-memory MongoDB server.
 */
let mongoMemoryServer;
/**
 * supertest takes our express app and runs it for us during tests. There is no need to start a ndoe environment.
 */
const request = supertest(app);

/**
 * before all tests: spin up the MongoMemoryServer and connect to it
 */
beforeAll(async () => {
  mongoMemoryServer = new MongoMemoryServer();
  const mongoDbUri = await mongoMemoryServer.getUri();
  await mongoose.connect(mongoDbUri, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false });
});

/**
 * After all tests disconnect from the MongoMemoryServer and stop it. All data, which was created during the test
 * will be gone. This will make sure, that tests are deterministic.
 */
afterAll(async () => {
  await mongoose.disconnect();
  await mongoMemoryServer.stop();
});

/**
 * describe() is used to group tests.
 */
describe("Category Routes", () => {
  /**
   * first a test object is created. We will use this object throughout the tests to persist data between tests.
   */
  const category = {
    name: "Food",
    description: "Delicious!",
  };

  /**
   * it() creates a new test case.
   */
  it("should get an empty array", async () => {
    /**
     * Send a request to "/categories".
     */
    const res = await request.get("/categories").send();

    /**
     * Check if the response has the expected data.
     */
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual([]);
  });

  it("should create a single category", async () => {
    const res = await request.post("/categories").send(category);

    expect(res.statusCode).toEqual(201);
    expect(res.body).toHaveProperty("id");
    expect(res.body).toHaveProperty("name", category.name);
    expect(res.body).toHaveProperty("description", category.description);

    category.id = res.body.id;
  });

  it("should get an array with one category", async () => {
    const res = await request.get("/categories").send();

    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual([category]);
  });

  it("should update a single category", async () => {
    const updatedDescription = "Magnificient!";
    const res = await request.patch(`/categories/${category.id}`).send({
      description: updatedDescription,
    });

    category.description = updatedDescription;

    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual(category);
  });

  it("should get a single category", async () => {
    const res = await request.get(`/categories/${category.id}`).send();

    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual(category);
  });

  it("should delete a single category", async () => {
    const res = await request.delete(`/categories/${category.id}`).send();

    expect(res.statusCode).toEqual(204);
    expect(res.body).toEqual({});
  });
});
