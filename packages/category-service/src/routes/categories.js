import express from "express";

import CategoryController from "../controllers/category";

/**
 * we define a new router, who maps the requests to the correct controllers
 */
const router = express.Router();

/**
 * These are the routes for the corresponding http methods. Example: calling "/categories" would call getCategories in
 * the CategoryController.
 */
router.get("/", CategoryController.getCategories);
router.post("/", CategoryController.createCategory);
router.get("/:categoryId", CategoryController.getCategory);
router.patch("/:categoryId", CategoryController.patchCategory);
router.delete("/:categoryId", CategoryController.deleteCategory);

export default router;
