import mongoose from "mongoose";

/**
 * an error handler, if the connection to the database is not established
 */
exports.handleDatabaseError = (databaseError) => {
  return (req, res, next) => {
    if (mongoose.connection.readyState != 1) {
      const error = new Error();
      error.status = 500;
      error.message = databaseError.message || "MongoError";
      error.description = databaseError.description || "Database unavailable";
      next(error);
    }
    /**
     * next() forwards the request to the next middleware
     */
    next();
  };
};

/**
 * Undefined Routes get an status of 404
 */
exports.handleUndefinedRoutes = (req, res, next) => {
  const error = new Error();
  error.status = 404;
  next(error);
};

/**
 * The handleError() gets an error object, which can be passed via next(error). This object will be evaluated.
 */
exports.handleError = (error, req, res, next) => {
  /**
   * common error types
   */
  const errorMessages = {
    400: "Bad Request",
    401: "Unauthorized Request",
    403: "Forbidden",
    404: "Not Found",
    405: "Method Not Allowed",
    500: "Internal Server Error",
    501: "Unimplemented",
  };

  /**
   * prepare the error object, which will be returned
   */
  error.status = error.status || 500;
  error.message = error.message || errorMessages[error.status] || "Undefined Error";
  error.description = error.description;
  error.errors = error.errors;

  /**
   * print the error, so we can see it in the logs
   */
  if (error.status === 500) {
    console.error(error);
  }

  /**
   * return the response to the client
   */
  res.status(error.status);
  res.json(error);
};
