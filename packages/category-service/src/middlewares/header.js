/**
 * Set's the header for the response. Also sets all allowed http methods checks
 */
exports.setAccessHeaders = (req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");

  /**
   * if the request method would be OPTIONS, an additional header with all allowed http methods would be set
   */
  if (req.method === "OPTIONS") {
    res.header("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE");
    return res.status(200).json({});
  }
  next();
};
