import mongoose from "mongoose";

import Category from "../models/category";

/**
 * The controller executes the CRUD operations for the database. Not all methods are fully described. You'll get the
 * idea by yoursself.
 */
exports.getCategories = async (req, res, next) => {
  try {
    /**
     * Find all categories and simply return the object ;)
     */
    const categories = await Category.find().exec();
    res.status(200).json(categories);
  } catch (e) {
    /**
     * If there would be an error, a new error object is created and passed to the error middleware.
     */
    const error = new Error();
    error.status = 500;
    error.description = e;
    next(error);
  }
};

exports.createCategory = async (req, res, next) => {
  /**
   * Create a new Category object. This object needs to match the Schema.
   */
  const category = new Category({
    _id: new mongoose.Types.ObjectId(),
    description: req.body.description,
    name: req.body.name,
  });

  try {
    await category.save();
    res.status(201).send(category);
  } catch (e) {
    const error = new Error();
    error.status = 500;
    error.description = e;
    next(error);
  }
};

exports.getCategory = async (req, res, next) => {
  try {
    const category = await Category.findById(req.params.categoryId).exec();
    if (!category) {
      const error = new Error();
      error.status = 404;
      error.description = "Category does not exist";
      return next(error);
    }
    return res.status(200).send(category);
  } catch (e) {
    const error = new Error();
    error.status = 500;
    error.description = e;
    next(error);
  }
};

exports.patchCategory = async (req, res, next) => {
  const patchQuery = {};

  for (const key in req.body) {
    patchQuery[key] = req.body[key];
  }

  try {
    const category = await Category.findOneAndUpdate(
      { _id: req.params.categoryId },
      { $set: patchQuery },
      { new: true }
    ).exec();
    if (!category) {
      const error = new Error();
      error.status = 404;
      error.description = "Category does not exist";
      return next(error);
    }
    res.status(200).json(category);
  } catch (e) {
    const error = new Error();
    error.status = 500;
    error.description = e;
    next(error);
  }
};

exports.deleteCategory = async (req, res, next) => {
  try {
    const category = await Category.findOneAndDelete({ _id: req.params.categoryId }).exec();
    if (!category) {
      const error = new Error();
      error.status = 404;
      error.description = "Category does not exist";
      return next(error);
    }
    res.status(204).send();
  } catch (e) {
    const error = new Error();
    error.status = 500;
    error.description = e;
    next(error);
  }
};
