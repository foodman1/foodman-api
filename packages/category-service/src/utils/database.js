import mongoose from "mongoose";

/**
 * Read in needed environment variables.
 */
const MONGO_ATLAS_USER = process.env.MONGO_ATLAS_USER;
const MONGO_ATLAS_PW = process.env.MONGO_ATLAS_PW;
const MONGO_ATLAS_URL = process.env.MONGO_ATLAS_URL;

/**
 * Connect to the database
 */
exports.connectToDatabase = async () => {
  try {
    await mongoose.connect(`mongodb+srv://${MONGO_ATLAS_USER}:${MONGO_ATLAS_PW}@${MONGO_ATLAS_URL}`, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    });
  } catch (e) {
    console.error(e.name, e.message);
    process.exit(-1);
  }
};

/**
 * Disconnect from the database. This is currently unused. We want our database to always be accessible.
 */
exports.disconnectFromDatabase = async () => {
  try {
    await mongoose.disconnect();
  } catch (e) {
    console.error(e.name, e.message);
    process.exit(-1);
  }
};
