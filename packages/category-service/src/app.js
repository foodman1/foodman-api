import express from "express";
import morgan from "morgan";

/**
 * Imports are prefixed with the type (middleware, contoller, router), to keep them distinguished
 */
import HeaderMiddleware from "./middlewares/header";
import ErrorMiddleware from "./middlewares/error";
import ValidationMiddleware from "./middlewares/validation";

import categoriesRouter from "./routes/categories";

/**
 * Initialize an empty express app. Each incoming request will run through the app until it's returned. If a middleware
 * notices, that a request is faulty it can be returned earlier.
 */
const app = express();

/**
 * Our first middleware. Each incoming request should get logged into the console. Morgan does this for us :) If we are
 * running the tests, the logging is disabled. Else this would flood our console and we couldn't read tests result
 * properly.
 */
if (process.env.NODE_ENV !== "test") {
  app.use(morgan("dev"));
}

/**
 * These middlewares check the request. If the requests contains data, it's attached to req.body. Those middlewares
 * transform the request into a JSON object and recognize the correct data types.
 */
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

/**
 * Our first customer middleware (see /middlewares/header.js). Checks the allowed http methods and headers (also CORS).
 */
app.use(HeaderMiddleware.setAccessHeaders);

/**
 * This custom middleware validates all requests against the OpenAPI spec. Example: If a request would contain invalid
 * body parametes, the error response will be automaticly set. This middleware requires the OpenAPi spec to be defined.
 * (see /config)
 */
app.use(ValidationMiddleware.validateRequestsAndResponses);

/**
 * After passing the initial middlewares, the request is passed to the router. The router forwards requests to the right
 * controller. Only if the route "/categories" would be called, the request is forwarded to the router (see /routes).
 */
app.use("/categories", categoriesRouter);

/**
 * The custom error middlewares handle all still unreturned requests. If an undefined route would be called, then the
 * first middleware would set the correct status code. The last middleware will format the error and finally return all
 * unresolved requests.
 */
app.use(ErrorMiddleware.handleUndefinedRoutes);
app.use(ErrorMiddleware.handleError);

export default app;
