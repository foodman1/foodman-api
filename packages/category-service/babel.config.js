/**
 * Setting the configuration for Babel and babel-jest to use the lates ECMAScript features, like
 * "import ... from ..." instead of "const ... = require(...)"
 */
module.exports = {
  presets: [["@babel/preset-env", { targets: { node: "current" } }]],
};
