import mongoose from "mongoose";
import { MongoMemoryServer } from "mongodb-memory-server";
import supertest from "supertest";

import AuthenticationMiddleware from "../middlewares/authentication";
jest.mock("../middlewares/authentication");

import app from "../app";

let mongoMemoryServer;
const request = supertest(app);

beforeAll(async () => {
  mongoMemoryServer = new MongoMemoryServer();
  const mongoDbUri = await mongoMemoryServer.getUri();
  await mongoose.connect(mongoDbUri, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false });
});

afterAll(async () => {
  await mongoose.disconnect();
  await mongoMemoryServer.stop();
});

describe("Me Routes", () => {
  const customerId = new mongoose.Types.ObjectId().toString();
  const cartId = new mongoose.Types.ObjectId().toString();
  const tableId = new mongoose.Types.ObjectId().toString();

  // the payment service is unable to create a jwt token
  // therefore the authentication middleware is mocked
  AuthenticationMiddleware.authenticate.mockImplementation((req, res, next) => {
    req.customerId = customerId;
    req.cartId = cartId;
    req.tableId = tableId;
    next();
  });

  it("should return unimplemented status", async () => {
    const res = await request.post("/me/payments").send();

    // this route is unimplemented and will fail as soon, as the implementation changes
    // this reminds the developer to update the test after implementation
    expect(res.statusCode).toEqual(501);
  });
});
