import express from "express";

import AuthenticationMiddleware from "../middlewares/authentication";

import PaymentController from "../controllers/payment";

const router = express.Router();

router.post("/", AuthenticationMiddleware.authenticate, PaymentController.createPaymentForAuthenticatedCustomer);

export default router;
