import app from "./app";

import DatabaseUtility from "./utils/database";

const SERVICE_NAME = process.env.PAYMENT_SERVICE_NAME;
const SERVICE_URL = process.env.PAYMENT_SERVICE_URL;
const SERVICE_PORT = process.env.PAYMENT_SERVICE_PORT;

DatabaseUtility.connectToDatabase();

app.listen(SERVICE_PORT, () => console.log(`${SERVICE_NAME} served on http://${SERVICE_URL}:${SERVICE_PORT} ...`));
