import mongoose from "mongoose";

const paymentSchema = mongoose.Schema({
  _id: mongoose.Schema.ObjectId,
  orders: {
    type: [String],
    required: true,
    validate: [(orders) => orders.length > 0, "{PATH} should be greater than 0"],
  },
  amount: { type: Number, required: true, min: 0 },
  customerId: { type: String, required: true },
  createdAt: { type: Date, required: true },
  __v: { type: Number, select: false },
});

paymentSchema.set("toJSON", {
  virtuals: true,
  transform: function (doc, ret) {
    delete ret._id;
  },
});

paymentSchema.set("toObject", {
  virtuals: true,
  transform: function (doc, ret) {
    delete ret._id;
  },
});

export default mongoose.model("Payment", paymentSchema);
