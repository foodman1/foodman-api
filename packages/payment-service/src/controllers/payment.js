import mongoose from "mongoose";
import got from "got";

import Payment from "../models/payment";

exports.createPaymentForAuthenticatedCustomer = async (req, res, next) => {
  return res.status(501).send();

  const ORDERS_URL = `http://${process.env.ORDER_SERVICE_NAME}:${process.env.ORDER_SERVICE_PORT}`;
  const orderQueryParams = `customerId=${req.customerId}&isPaid=false`;

  try {
    // get unpaid orders for customer from order-service
    const orders = await got.get(`${ORDERS_URL}/orders?${orderQueryParams}`).json();

    let totalAmount = 0;
    const orderIds = [];
    const jsonPatch = [];
    orders.map((order) => {
      // get order ids
      orderIds.push(order.id);
      // create JSON-Patch
      jsonPatch.push({ op: "replace", path: `/orders/${order.id}/paidAt`, value: new Date() });
      // calulate total amount
      order.items.map((orderItem) => (totalAmount += orderItem.quantity * orderItem.product.price));
    });

    // create payment
    const payment = new Payment({
      _id: new mongoose.Types.ObjectId(),
      orders: orderIds,
      amount: totalAmount,
      customerId: req.customerId,
      createdAt: new Date(),
    });
    await payment.save();

    // TODO: set paidAt of orders
    res.status(201).send(payment);
  } catch (e) {
    const error = new Error();
    error.status = 500;
    error.description = e;
    next(error);
  }
};
