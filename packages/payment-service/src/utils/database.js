import mongoose from "mongoose";

const MONGO_ATLAS_USER = process.env.MONGO_ATLAS_USER;
const MONGO_ATLAS_PW = process.env.MONGO_ATLAS_PW;
const MONGO_ATLAS_URL = process.env.MONGO_ATLAS_URL;

exports.connectToDatabase = async () => {
  try {
    await mongoose.connect(`mongodb+srv://${MONGO_ATLAS_USER}:${MONGO_ATLAS_PW}@${MONGO_ATLAS_URL}`, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    });
  } catch (e) {
    console.error(e.name, e.message);
    process.exit(-1);
  }
};

exports.disconnectFromDatabase = async () => {
  try {
    await mongoose.disconnect();
  } catch (e) {
    console.error(e.name, e.message);
    process.exit(-1);
  }
};
