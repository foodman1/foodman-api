# Foodman API

An microservice-based JavaScript API for Foodman. Foodman is a service, which allows customers of a restaurant to view, order and pay food and beverages via an mobile application.

[[_TOC_]]

## Requirements

- [Docker](https://docs.docker.com/engine/install/) (and [Docker Compose](https://docs.docker.com/compose/install/) on Linux) to **run** the project
- [Node.js](https://nodejs.org/) to **develop** and **test** the project

## Getting Started

Clone the project into a local directory. The project requires a `.env` file, which keeps all environment variables. :red_circle: **This file is not included in the repository and needs to be added by YOU.** :red_circle:

All below listed commands should be executed from root of project directory, if not stated otherwise.

### Setting up an production-like environment

1. `docker-compose up -d --build` to spin up all docker containers
   - Access endpoints via http://localhost
   - Access an _work-in-progress_ api documentation at http://localhost:9000
   - Access the reverse proxy dashboard of [Traefik](https://doc.traefik.io/traefik/) at http://localhost:8080
2. `docker-compose down` to stop all docker containers

### Setting up an development environment

It's recommended to read carefully through the whole documentation, before starting to work with this project.

1. `npm install` to install project dependencies
2. `npx lerna bootstrap` to install dependencies of each microservice
3. `npm run start:dev` to start an auto-refreshing development environment
4. Develop with :heart: and :coffee:
5. `npm run stop` to shut down all services

## Running Unit Tests

This repository houses all microservices under `./packages`.

- `npm test` in the project root directory to run tests for all services
- `npm test` in the service directory (`./packages/<service>`) to run tests for a specific service

## Editor Recommendation and Setup

Developing in this project works best with the right editor. The recommended choice is [Visual Studio Code](https://code.visualstudio.com/) with the following extensions enabled:

- [Docker](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker) to manage and debug containers
- [EditorConfig for VS Code](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig) to apply `.editorconfig` settings to VS Code
- [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode) to apply `.prettierrc` formatter settings to VS Code
- [DotENV](https://marketplace.visualstudio.com/items?itemName=mikestead.dotenv) for syntax highlighting in `.env` files
- **For WSL Users:** [Remote Development](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack)

and the following workspace settings:

```json
{
  "editor.defaultFormatter": "esbenp.prettier-vscode",
  "editor.formatOnSave": true
}
```

## Further Reading

The developer documentation doesn't stop here. Dive in deeper into the project with one of the following topics:

- [PROJECT](./docs/PROJECT.md) - A deeper look into the microservice project and functionalities of the monorepo
- [SERVICES](./docs/SERVICES.md) - Learn more about the service dependencies and functionality
- [API DOCUMENTATION](./docs/API_DOCUMENTATION.md) - Since the OpenAPI specification is still _work in progress_, this is full list on all usable endpoints
- [CI/CD](./docs/CI_CD.md) - Pipeline makes beep boop :computer:

## Troubleshooting and Known Issues

### Development environment is not auto-refreshing on save

Are you working on Windows? Windows file system notifications (like _saving_) can't be mounted into WSL. **A workaround is to move the project into the WSL file system.**

### Tests can not be executed from within the docker container

Tests use an in-memory MongoDB and MongoDB is currently not supported by alpine linux (services depend on `node:lts-alpine`), see [here](https://github.com/nodkz/mongodb-memory-server#docker-alpine). **Run test in your local development environment or wait for the CI/CD pipeline** (which uses `node:lts`) **to run**.
